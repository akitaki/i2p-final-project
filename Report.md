---
# Pandoc settings
documentclass: scrbook
fontsize: 12pt
indent: true
geometry: margin=2cm
toc: true
toc-depth: 1
classoption: openany

# Content
title: Final Project Report
subtitle: I2P Course at NTHU Department of CS
author: |
        | 107021129 黃明瀧
        | 107062110 顏廷宇
        | 107062203 張家瑜
---

# Basic Skills

## Meaning of Compiler Flags

> 請解釋下列編譯指令，每一個參數代表的意涵 (10%)

```bash
g++-8 -std=c++17 -O2 -Wall -Wextra -fPIC \
    -I./ -shared AITemplate/Porting.cpp -o ./build/a1.so
```

1. `-std=c++17`

    This flag tells the compiler toolchain to compile with C++17 language standard.
    The C++ language has been standarized and published as several revisions throughout the years of development.
    Newer revisions of standard may introduce new language constructs and make additions to the standard library,
    but may also deprecate or even remove old features from the language.

2. `-O2`

    This flag specifies the optimization level.
    Most GCC-compatible compiler front-ends (such as clang for LLVM) have the following optimization levels[^1].

    a. `-O0`, the "no optimization" level.
        This is the default level, which does absolutely no compile time optimizations.
        This level is ecspecially suitable for faster debug builds.

    b. `-O1`, the "moderate optimization" level.
        With this flag on, the compiler will try to make some optimizations without degrading compilation time significantly.

    c. `-O2`, the "full optimization" level.
        This flag enables nearly all supported optimizations except for those that trade binary size with speed.

    d. `-O3` enables all the optimizations in `-O2` and also turns on some aggresive optimization techniques
        such as loop unrolling and more vectorization.

3. `-Wall` and `-Wextra`

    The `-Wall` flag enables most compiler warnings for questionable code that are easy to avoid[^2].
    The `-Wextra` enables even more types of warnings that are not enabled by the `-Wall` flag.

4. `-fPIC`

    This codegen flag tells the compiler to generate position-independent code if it's supported by the target[^3].
    Position-independent code is a segment of machine code that can be placed at any address in the virtual memory space.
    This flag adds some indirection overheads at load time and / or runtime.
    The `-fPIC` argument is necessary for building shared libraries,
    since the library can't predict the base address of itself when loaded.
    
5. `-I./`

    The `-I` flag is for specifying search directories for header files.
    `-I./` means to add the current directory into the list of search directories so that header files in this directory can be `#include <...>`ed.

6. `-shared`

    Produce a shared object.
    
7. `AITemplate/Porting.cpp -o ./build/a1.so`

    These arguments specify the input and output filenames.

[^1]: *3.11 Options That Control Optimization*, <https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html>

[^2]: *3.8 Options to Request or Suppress Warnings*, <https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html>

[^3]: *3.17 Options for Code Generation Conventions*, <https://gcc.gnu.org/onlinedocs/gcc/Code-Gen-Options.html>

## Meaning of the `call` Function Template

> 請解釋 Game.h 裡面 `call` 函數的功能 (5%)

One of the `call` member functions (there are two) looks like this.

```C
template <typename Func, typename... Args,
          std::enable_if_t<std::is_void<
              std::invoke_result_t<Func, AIInterface, Args...>
          >::value, int> = 0>
void call(Func func, AIInterface* ptr, Args... args) {
    std::future_status status;
    auto val = std::async(std::launch::async, func, ptr, args...);
    status   = val.wait_for(std::chrono::milliseconds(m_runtime_limit));

    if (status != std::future_status::ready) {
        exit(-1);
    }
    val.get();
}
```

From a big picture of view,
this template function calls arbitrary function on the given `AIInterface*`,
and wait for at most one second before collecting the result.

### The Template Arguments

There are several things in the template argument list.

1. `typename Func` declares `Func` as an template parameter. All subsequent usage of `Func` refers to this type.
2. `typename... Args` declares a *parameter pack*,
which makes this template variadic,
meaning that the `Args... args` part in the function can contain zero or more elements.
3. The `std::enable_if_t<...>` nameless value parameter abuses SFINAE to limit this template to be instantiated only for `Func` types that both
    1. can be invoked with the provided `args` and
    2. returns `void`
. The trailing `= 0` gives this nameless parameter a default value so that callers doesn't need to specify it explicitly.

### The Function Body

First, a variable of type `std::future_status` is declared to hold the (synchronized) result of the `async` task.
`std::async` is then used to call the function inside the shared library with specified parameters.
After that, we call `std::future<T>::wait_for` to give the player agent at most 1 second to complete its task.

Note that inside the project template given by the TAs,
`exit(-1)` is invoked upon any failed attempt by a player agent to complete its task within the time limit.
However, in reality we need to implement the logic to judge the player agent lost.
This causes a potential problem if the player agent never returns,
since the `std::future`'s destructor blocks the main thread indefinitely[^4].

[^4]: Answer to *Confusion about threads launched by std::async with `std::launch::async` parameter*, Stack Overflow, <https://stackoverflow.com/a/32517201>

## Shared Libraries

> 請解釋什麼是 Shared library，為何需要 Shared library ，在 Windows 系統上有類似的東西嗎？(10%)

Shared libraries are shared object files (`*.so`) that can be reused by multiple processes,
as opposed to static libraries that must be linked into the program image beforehand.
It's adopted by most modern operating systems to ease resource usage as well as maintainence problems.

During linking, the linker must include full copies of static libraries into the runnable image.
As a result, memory and secondary storage space are wasted to store multiple sections of the same library code.
Also, whenever we want to update a static library, everything relying on the library needs to be built again.
Shared libraries mitigate these issues by either loading library symbols at either load-time (by the program loader) or run-time (using APIs provided by the operating system).
In this way, the OS may take advantage of virtual memory and point multiple usages of a shared library to the same shared pages, thereby reducing memory consumption dramatically.
In addition, updating a shared library is as simple as replacing the old `*.so` file with a new one.

Shared libraries also come with some cost, most notably compatibility issues.
Dynamically linked programs rely on having compatible libraries on the target system.
If a library is changed or removed, the program may not work anymore.
Static libraries don't have this issue because they're baked into the program itself.
This situation makes it difficult to compile a "universal" binary for Linux distributions,
for example, since glibc is not forward-compatible.
Running a binary built against newer versions of glibc on an older system does not work.

On Microsoft Windows operating systems,
the standard format for shared libraries  is named as *dynamic link libraries* (DLLs).

# The Final Project

## Our AI Algorithm

## Division of Work and Scheduling

## Feedbacks and Lessons Learned
