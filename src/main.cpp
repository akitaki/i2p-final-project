#include "UI/tui.h"
#include "UI/testui.h"

#include <string>
#include <csignal>
#include <memory>
#include <optional>

namespace {
std::optional<ui::TUI*> tui;
}

extern "C" void windowSizeHandler([[maybe_unused]] int sig) {
    if (tui.has_value()) {
        tui.value()->update(true);
    }
}

int main(int argc, char** argv) {
    if (argc > 1 && std::string(argv[1]) == "-t") {
        ui::TestUI testui{};
        testui.run();
        return 0;
    }

    std::signal(SIGWINCH, windowSizeHandler);
    tui = new ui::TUI();
    (*tui)->start();

    delete *tui;

    return 0;
}
