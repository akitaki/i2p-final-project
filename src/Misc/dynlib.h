#ifndef DYNLIB_H
#define DYNLIB_H

#include "./TA/Wrapper/ai.h"
#include "./TA/Wrapper/content.h"

#ifdef __unix__
#include <dlfcn.h>
#else
#error "Currently the shared library interface only support POSIX platforms"
#endif

#include <stdexcept>
#include <string>

/**
 * @brief Support utilities that don't belong to certain part of the project
 */
namespace util {

/**
 * @brief RAII wrapper around `Content` that automatically delete the AI handle
 * close the shared library on destruction
 */
class Dynlib {
  public:
    /**
     * @brief Try to load shared library with path `path` and load get an
     * instance of its exported AI agent
     */
    Dynlib(std::string path) noexcept(false) {
        content.handle = dlopen(path.c_str(), RTLD_LAZY);

        if (content.handle == nullptr) {
            const auto errString =
                std::string("Failed to open dynamic library:\n") + dlerror();
            throw std::runtime_error(errString);
        }

        auto tryLoad = [&](const char* func) -> void* {
            void* func_ptr = dlsym(content.handle, func);

            if (func_ptr == nullptr) {
                const auto errString =
                    std::string("Failed to get address of function:\n") +
                    dlerror();
                throw std::runtime_error(errString);
            }

            return func_ptr;
        };

        content.getai =
            reinterpret_cast<decltype(content.getai)>(tryLoad("getai"));

        this->ai = reinterpret_cast<AIInterface*>(content.getai());
    }

    ~Dynlib() {
        delete ai;
        dlclose(content.handle);
    }

    AIInterface* getAI() { return ai; }

  private:
    Content content;
    Dynlib(const Dynlib&) = delete;
    AIInterface* ai       = nullptr;
};
} // namespace util

#endif /* ifndef DYNLIB_H */
