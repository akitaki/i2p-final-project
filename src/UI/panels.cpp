#include "./panels.h"
#include "./tui.h"

namespace {
constexpr std::array<const wchar_t*, 13> boardGridArr = {
    L"┏━━━┯━━━┯━━━┓",
    L"┃   │   │   ┃",
    L"┃   │   │   ┃",
    L"┃   │   │   ┃",
    L"┠───┼───┼───┨",
    L"┃   │   │   ┃",
    L"┃   │   │   ┃",
    L"┃   │   │   ┃",
    L"┠───┼───┼───┨",
    L"┃   │   │   ┃",
    L"┃   │   │   ┃",
    L"┃   │   │   ┃",
    L"┗━━━┷━━━┷━━━┛"};
}

namespace ui {

/* IPanel member functions */

void IPanel::draw() {
    if (frameShouldRedraw) {
        drawFrame();
        frameShouldRedraw = false;
    }
    drawContent();
}

void IPanel::drawFrame() const {
    if (!framed)
        return;

    if (focused) {
        stdoutwss << ui::bold << ui::green;
    }

    // Top frame
    cursor::move(dim.row, dim.col);
    ui::stdoutwss << frameChars->topLeft;
    for (size_t col = 2; col < dim.width; col++) {
        ui::stdoutwss << frameChars->top;
    }
    ui::stdoutwss << frameChars->topRight;

    // Side frame
    for (size_t row = dim.row + 1; row < dim.row + dim.height - 1; row++) {
        cursor::move(row, dim.col);
        ui::stdoutwss << frameChars->left;
        cursor::move(row, dim.col + dim.width - 1);
        ui::stdoutwss << frameChars->right;
    }

    // Bottom frame
    cursor::move(dim.row + dim.height - 1, dim.col);
    ui::stdoutwss << frameChars->botLeft;
    for (size_t i = 2; i < dim.width; i++) {
        ui::stdoutwss << frameChars->bot;
    }
    ui::stdoutwss << frameChars->botRight;

    // Title
    if (!this->title.has_value())
        return;
    const auto& title = this->title.value();
    cursor::move(dim.row, dim.col + 2);
    ui::stdoutwss << title;

    if (focused) {
        stdoutwss << ui::reset;
    }
}

PanelDim IPanel::getInnerDim() const {
    return PanelDim(dim.row + 1, dim.col + 1, dim.height - 2, dim.width - 2);
}

/* LogPanel member functions */

void LogPanel::drawContent() {
    const auto dim       = this->getInnerDim();
    const auto start_row = dim.row + dim.height - 1;
    const auto end_row   = dim.row;

    auto it = logs.crbegin();
    for (auto row = start_row; row >= end_row; row--) {
        if (it == logs.crend())
            break;

        cursor::move(row, dim.col);
        cursor::move(row, dim.col);

        auto printWidth = std::min(it->first.size(), dim.width);
        ui::stdoutwss << it->first.substr(0, printWidth);

        // Erase over old content
        for (size_t i = 0; i < dim.width - printWidth; i++)
            ui::stdoutwss << ' ';

        std::advance(it, 1);
    }
}

void LogPanel::clearLogs() { logs.clear(); }

/* BoardPanel member functions */

void BoardPanel::setBoard(TA::UltraBoard ub) { this->ub = ub; }

void BoardPanel::drawContent() {
    const auto dim = this->getInnerDim();

    if (dim.height < 13 || dim.width < 13) {
        cursor::move(dim.row, dim.col);
        ui::stdoutwss << L"Window";
        cursor::move(dim.row + 1, dim.col);
        ui::stdoutwss << L"is too";
        cursor::move(dim.row + 2, dim.col);
        ui::stdoutwss << L"small";
        return;
    }

    const auto [centerRow, centerCol] = dim.center();
    const auto baseRow                = centerRow - 7;
    const auto baseCol                = centerCol - 7;

    cursor::move(baseRow, baseCol);

    for (const auto line : boardGridArr) {
        ui::stdoutwss << line;
        cursor::down(1);
        cursor::left(13);
    }

    for (size_t i = 0; i < 3; i++) {
        for (size_t j = 0; j < 3; j++) {
            const auto& subBoard  = ub.sub(i, j);
            const auto subBaseRow = i * 4 + 1 + baseRow;
            const auto subBaseCol = j * 4 + 1 + baseCol;

            const auto color = tagToColor(subBoard.getWinTag());

            cursor::move(subBaseRow, subBaseCol);
            ui::stdoutwss << color;
            for (size_t x = 0; x < 3; x++) {
                for (size_t y = 0; y < 3; y++) {
                    if (focused &&
                        j * 3 + y == static_cast<size_t>(cursorAt.first) &&
                        i * 3 + x == static_cast<size_t>(cursorAt.second)) {
                        ui::stdoutwss << ui::reverse
                                      << tagToWchar(subBoard.state(x, y))
                                      << ui::reset << color;
                    } else {
                        ui::stdoutwss << tagToWchar(subBoard.state(x, y));
                    }
                }
                cursor::down(1);
                cursor::left(3);
            }
            ui::stdoutwss << reset;
        }
    }
}

/* CtrlPanel member functions */

CtrlPanel::CtrlPanel(PanelDim dim, util::Sender<CallbackEvent> uiCallbackSender)
    : IPanel(dim, true, FrameCharSet{}, L"Controls"),
      uiCallbackSender(uiCallbackSender) {

    // Build menu items
    items.push_back(
        std::make_unique<SwitchCtrlItem>(L"1st AI", dim.width, true));
    items.push_back(
        std::make_unique<SwitchCtrlItem>(L"2nd AI", dim.width, true));
    items.push_back(std::make_unique<ButtonCtrlItem>(
        L"[Start Game]", dim.width, [](CtrlPanel& ctrlPanel) {
            ctrlPanel.sendStartGame();
        }));
    items.push_back(std::make_unique<ButtonCtrlItem>(
        L"[Quit]", dim.width, [](CtrlPanel& ctrlPanel) {
            ctrlPanel.sendQuit();
        }));

    // Set first item to focused
    items[0]->focused = true;
}

void CtrlPanel::setDim(PanelDim dim) {
    IPanel::setDim(dim);

    // Pass the width information to all the menu items
    for (auto&& item : items) {
        item->width = dim.width;
    }
}

void CtrlPanel::onArrowEvent(CallbackEvent::ArrowPress event) {
    // Move down / up if possible
    if (event.value == CallbackEvent::ArrowPress::Down) {
        if (curItem != items.size() - 1) {
            items[curItem]->focused = false;
            curItem += 1;
            items[curItem]->focused = true;
        }
    } else if (event.value == CallbackEvent::ArrowPress::Up) {
        if (curItem != 0) {
            items[curItem]->focused = false;
            curItem -= 1;
            items[curItem]->focused = true;
        }
    }
}

void CtrlPanel::drawContent() {
    const auto dim = getInnerDim();
    ui::stdoutwss << ch;
    for (size_t i = 0; i < items.size(); i++) {
        items[i]->draw(dim.row + i, dim.col);
    }
}

std::pair<std::optional<std::string>, std::optional<std::string>>
CtrlPanel::getLibNames() {
    std::pair<std::optional<std::string>, std::optional<std::string>> libNames;

    if (static_cast<SwitchCtrlItem*>(items[0].get())->active) {
        libNames.first = "./a1.so";
    }

    if (static_cast<SwitchCtrlItem*>(items[1].get())->active) {
        libNames.second = "./a2.so";
    }

    return libNames;
}
} // namespace ui
