#include "./tui.h"

#include "../Game/game.h"
#include "../Misc/dynlib.h"
#include "./cursor.h"

#ifdef __unix__
#include <termios.h>
#else
#error "Currently the UI only support Unix terminals"
#endif

#include <array>
#include <cwchar>
#include <iostream>
#include <memory>
#include <sstream>
#include <thread>
#include <type_traits>
#include <variant>

namespace ui {

/* Free functions */

void flush() {
    cursor::move(0, 0);
    ui::stdoutwss << hideCursor;
    std::wcout << ui::stdoutwss.str();
    std::wcout.flush();
    ui::stdoutwss.str(L"");
}

wchar_t tagToWchar(TA::BoardInterface::Tag tag) {
    switch (tag) {
    case TA::BoardInterface::Tag::O:
        return L'O';
    case TA::BoardInterface::Tag::X:
        return L'X';
    case TA::BoardInterface::Tag::Tie:
        return L'=';
    case TA::BoardInterface::Tag::None:
        return L'-';
    default:
        throw std::runtime_error("Unknown BoardInterface::Tag variant");
    }
}

const wchar_t* tagToColor(TA::BoardInterface::Tag tag) {
    switch (tag) {
    case TA::BoardInterface::Tag::O:
        return blue;
    case TA::BoardInterface::Tag::X:
        return red;
    case TA::BoardInterface::Tag::Tie:
        return yel;
    case TA::BoardInterface::Tag::None:
        return reset;
    default:
        throw std::runtime_error("Unknown BoardInterface::Tag variant");
    }
}

/* TUI member functions */

TUI::TUI()
    : wsize(), events(util::createChannel<ui::CallbackEvent>().second),
      sender(events.subscribe()),
      boardPanel(boardPanelDim(wsize), TA::UltraBoard(), events.subscribe()),
      logPanel(logPanelDim(wsize)),
      ctrlPanel(ctrlPanelDim(wsize), events.subscribe()) {

    // Focus on control panel
    ctrlPanel.setFocused(true);

    // Enable Unicode output
    std::locale::global(std::locale("en_US.utf8"));
    ui::stdoutwss.imbue(std::locale());
}

TUI::~TUI() {
    // Join all game threads
    for (auto&& worker : workers) {
        std::wcout << L"Waiting for worker thread...\n";
        worker.join();
    }

    // Clear the output
    cursor::clear();
    flush();
    std::wcout << showCursor;
}

void TUI::start() {
    // Clear the screen
    cursor::clear();

    // Char input thread
    std::thread(
        [](util::Sender<ui::CallbackEvent>&& sender) {
            // Disable STDIN buffering
            // For more info, consult the Linux Programmer's Manual.
            termios terminfo;
            tcgetattr(0, &terminfo);

            // Set various flags.
            //
            // This collection of flags are based on "raw" mode in stty
            // (coreutils)
            // https://github.com/coreutils/coreutils/blob/master/src/stty.c

            terminfo.c_iflag &=
                ~(IGNBRK | BRKINT | IGNPAR | PARMRK | INPCK | ISTRIP | INLCR |
                  IGNCR | ICRNL | IXON | IXOFF | IUCLC | IXANY | IMAXBEL);
            terminfo.c_oflag &= ~(OPOST);
            terminfo.c_lflag &= ~(ECHO | ISIG | ICANON | XCASE);
            terminfo.c_cc[VMIN]  = 1;
            terminfo.c_cc[VTIME] = 0;

            tcsetattr(0, TCSANOW, &terminfo);

            // Check if the changes are performed successfully
            termios terminfoAfter;
            tcgetattr(0, &terminfoAfter);
            if (terminfo.c_iflag != terminfoAfter.c_iflag ||
                terminfo.c_oflag != terminfoAfter.c_oflag ||
                terminfo.c_lflag != terminfoAfter.c_lflag ||
                terminfo.c_cc[VMIN] != terminfoAfter.c_cc[VMIN] ||
                terminfo.c_cc[VTIME] != terminfoAfter.c_cc[VTIME]) {
                sender.send(ui::CallbackEvent::newLog(
                    LogBuilder() << L"Raw mode setup failed"));
            }

            // Input loop
            wchar_t input;
            while (true) {
                input = std::getwchar();
                if (input == L'\033') {
                    // Discard L'['
                    getwchar();
                    auto arrowCode = getwchar();
                    if (arrowCode >= L'A' && arrowCode <= L'D') {
                        sender.send(CallbackEvent::newArrowPress(arrowCode));
                    }
                } else if (input == L'\t') {
                    sender.send(CallbackEvent::newTabPress());
                } else {
                    sender.send(CallbackEvent::newWcharInput(input));
                }
            }
        },
        events.subscribe())
        .detach();

    // Handle event from input thread
    const auto handleWcharInput = [this](wchar_t ch) {
        if (focusIn == FocusIn::Ctrl) {
            ctrlPanel.onCharInput(ch);
            ctrlPanel.draw();
            flush();
        } else if (focusIn == FocusIn::Board) {
            boardPanel.onCharInput(ch);
            boardPanel.draw();
            flush();
        }

        return ch == L'q';
    };

    const auto handleArrowPress = [this](CallbackEvent::ArrowPress event) {
        if (focusIn == FocusIn::Ctrl) {
            ctrlPanel.onArrowEvent(event);
            ctrlPanel.draw();
            flush();
        } else if (focusIn == FocusIn::Board) {
            boardPanel.onArrowEvent(event);
            boardPanel.draw();
            flush();
        }

        return false;
    };

    const auto handleStartGame =
        [this]([[maybe_unused]] CallbackEvent::StartGame event) {
            if (isGameRunning) {
                this->sender.send(CallbackEvent::newLog(
                    LogBuilder() << L"Game is already running"));
                return false;
            }

            this->sender.send(CallbackEvent::newLog(LogBuilder() << L""));
            this->sender.send(
                CallbackEvent::newLog(LogBuilder() << L"Starting game"));

            auto [libpathA, libpathB] = ctrlPanel.getLibNames();
            startGame(libpathA, libpathB);

            return false;
        };

    const auto handleLog = [this](CallbackEvent::Log event) {
        logPanel.push(event.str, 0);
        logPanel.draw();
        flush();
        return false;
    };

    const auto handleUpdateBoard = [this](CallbackEvent::UpdateBoard event) {
        boardPanel.setBoard(event.board);
        boardPanel.draw();
        flush();
        return false;
    };

    const auto handleClearLog = [this]() {
        logPanel.clearLogs();
        logPanel.draw();
        flush();
        return false;
    };

    const auto handleTabPress = [this]() {
        focusIn = (focusIn == FocusIn::Ctrl) ? FocusIn::Board : FocusIn::Ctrl;

        ctrlPanel.setFocused(focusIn == FocusIn::Ctrl);
        ctrlPanel.markFrameShouldRedraw();
        boardPanel.setFocused(focusIn == FocusIn::Board);
        boardPanel.markFrameShouldRedraw();

        ctrlPanel.draw();
        boardPanel.draw();
        flush();

        return false;
    };

    const auto handleUserMove = [this](CallbackEvent::UserMove event) {
        if (gameCtrlSender.has_value()) {
            gameCtrlSender->send(
                game::Game::CtrlMsg::newUserMove(event.x, event.y));
        }
        return false;
    };

    const auto handleUserTurn =
        [this]([[maybe_unused]] CallbackEvent::UserTurn event) {
            boardPanel.setUserTurn(true);
            return false;
        };

    // Initial UI setup
    cursor::clear();
    update();

    // Main event loop
    while (true) {
        const auto event          = events.recv();
        const bool shouldShutdown = std::visit(
            [&](auto&& event) {
                using T = std::decay_t<decltype(event)>;
                if constexpr (std::is_same_v<T, CallbackEvent::WcharInput>) {
                    return handleWcharInput(event.ch);
                } else if constexpr (std::is_same_v<
                                         T,
                                         CallbackEvent::ArrowPress>) {
                    return handleArrowPress(event);
                } else if constexpr (std::is_same_v<
                                         T,
                                         CallbackEvent::StartGame>) {
                    return handleStartGame(event);
                } else if constexpr (std::is_same_v<T, CallbackEvent::Log>) {
                    return handleLog(event);
                } else if constexpr (std::is_same_v<
                                         T,
                                         CallbackEvent::UpdateBoard>) {
                    return handleUpdateBoard(event);
                } else if constexpr (std::is_same_v<
                                         T,
                                         CallbackEvent::ClearLog>) {
                    return handleClearLog();
                } else if constexpr (std::is_same_v<
                                         T,
                                         CallbackEvent::UserTurn>) {
                    return handleUserTurn(event);
                } else if constexpr (std::is_same_v<
                                         T,
                                         CallbackEvent::TabPress>) {
                    return handleTabPress();
                } else if constexpr (std::is_same_v<
                                         T,
                                         CallbackEvent::UserMove>) {
                    return handleUserMove(event);
                } else if constexpr (std::is_same_v<
                                         T,
                                         CallbackEvent::Shutdown>) {
                    logPanel.push(L"Quitting game runner...", 0);
                    logPanel.draw();
                    flush();

                    // Ask game thread to shutdown
                    if (gameCtrlSender.has_value()) {
                        gameCtrlSender->send(
                            game::Game::CtrlMsg::newQuitGame());
                    }

                    return true;
                } else {
                    return false;
                }
            },
            event.event);

        if (shouldShutdown) {
            break;
        }
    }
}

void TUI::update(bool didWinSizeChange) {
    if (didWinSizeChange) {
        boardPanel.markFrameShouldRedraw();
        logPanel.markFrameShouldRedraw();
        ctrlPanel.markFrameShouldRedraw();
        cursor::clear();
    }

    wsize.update();
    boardPanel.setDim(boardPanelDim(wsize));
    boardPanel.draw();

    logPanel.setDim(logPanelDim(wsize));
    logPanel.draw();

    ctrlPanel.setDim(ctrlPanelDim(wsize));
    ctrlPanel.draw();

    flush();
}

void TUI::update(TA::UltraBoard ub) {
    this->boardPanel.setBoard(ub);
    this->update();
}

void TUI::startGame(
    std::optional<std::string> libpathA, std::optional<std::string> libpathB) {
    if (isGameRunning)
        return;

    isGameRunning = true;

    const auto sender = events.subscribe();
    auto gameInstance = game::Game(sender);

    gameCtrlSender = gameInstance.sender();

    workers.emplace_back([=, gameInstance(std::move(gameInstance))]() mutable {
        std::optional<std::unique_ptr<util::Dynlib>> libA, libB;

        try {
            if (libpathA.has_value()) {
                libA = std::make_unique<util::Dynlib>(*libpathA);
            }

            if (libpathB.has_value()) {
                libB = std::make_unique<util::Dynlib>(*libpathB);
            }
        } catch (std::exception& e) {
            std::cerr << e.what();
            exit(1);
        }

        if (libA.has_value()) {
            gameInstance.setPlayer1(libA.value()->getAI());
        }

        if (libB.has_value()) {
            gameInstance.setPlayer2(libB.value()->getAI());
        }

        gameInstance.run();

        isGameRunning = false;
    });
}

} // namespace ui
