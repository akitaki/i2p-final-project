#include "./testui.h"

#include "../Game/game.h"
#include "../Misc/channel.h"
#include "../Misc/dynlib.h"
#include "./uicallback.h"
#include "TA/board.h"
#include "UI/tui.h"

#include <array>
#include <cstddef>
#include <iostream>
#include <memory>
#include <string>
#include <map>

namespace ui {
TestUI::TestUI() {}
void TestUI::run() {
    auto [send, recv] = util::createChannel<CallbackEvent>();
    std::array<std::optional<std::unique_ptr<util::Dynlib>>, 2> libs;
    std::string cmd;
    size_t rounds = 1;

    std::map<TA::BoardInterface::Tag, size_t> table;
    table[TA::BoardInterface::Tag::O]    = 0;
    table[TA::BoardInterface::Tag::X]    = 0;
    table[TA::BoardInterface::Tag::Tie]  = 0;
    table[TA::BoardInterface::Tag::None] = 0;

    while (std::cout << "prompt > ", std::cin >> cmd) {
        if (cmd == "lib") {
            size_t idx;
            std::string path;
            std::cin >> idx >> path;

            libs[idx] = std::make_unique<util::Dynlib>(path);
        } else if (cmd == "rounds") {
            std::cin >> rounds;
        } else if (cmd == "run") {
            table[TA::BoardInterface::Tag::O]    = 0;
            table[TA::BoardInterface::Tag::X]    = 0;
            table[TA::BoardInterface::Tag::Tie]  = 0;
            table[TA::BoardInterface::Tag::None] = 0;

            while (rounds--) {
                game::Game game(send);
                game.setPlayer1((*libs[0])->getAI());
                game.setPlayer2((*libs[1])->getAI());
                const auto res = game.run();
                if (res.has_value())
                    table[*res]++;

                std::cout << "O: " << table[TA::BoardInterface::Tag::O] << "\n";
                std::cout << "X: " << table[TA::BoardInterface::Tag::X] << "\n";
                std::cout << "T: " << table[TA::BoardInterface::Tag::Tie]
                          << "\n";
                std::cout << "N: " << table[TA::BoardInterface::Tag::None]
                          << "\n";
            }
        } else if (cmd == "q") {
            return;
        }
    }
}
} // namespace ui
