#ifndef PANELS_H
#define PANELS_H

#include "../Misc/channel.h"
#include "../TA/ultra_board.h"
#include "./cursor.h"
#include "./stdoutbuf.h"
#include "./uicallback.h"

#include <functional>
#include <optional>

namespace ui {
struct PanelDim {
    size_t height = 0;
    size_t width  = 0;
    size_t row    = 0;
    size_t col    = 0;

    PanelDim() {}

    PanelDim(size_t row, size_t col, size_t height, size_t width)
        : height(height), width(width), row(row), col(col) {}

    std::pair<size_t, size_t> center() const {
        return {row + height / 2, col + width / 2};
    }
};

class IPanel {
  public:
    IPanel(
        PanelDim dim,
        bool framed,
        std::optional<FrameCharSet> frameChars = FrameCharSet{},
        std::optional<std::wstring> title      = std::nullopt)
        : dim(dim), framed(framed), frameChars(frameChars), title(title) {}

    inline virtual ~IPanel() {}

    /** @brief Draw (update) the panel to STDOUT */
    void draw();

    /** @brief Update the outer dimension */
    virtual inline void setDim(PanelDim dim) { this->dim = dim; }

    /** @brief Get the outer dimension */
    inline PanelDim getDim() const { return this->dim; }

    /** @brief Update focus status */
    inline void setFocused(bool focused) { this->focused = focused; }

    /** @brief Get the focus status */
    inline bool getFocused() const { return focused; }

    /** @brief Mark this panel as needing full redraw */
    inline void markFrameShouldRedraw() { frameShouldRedraw = true; }

  protected:
    virtual void drawContent() = 0;

    void drawFrame() const;

    PanelDim getInnerDim() const;

    PanelDim dim;
    bool framed;
    bool frameShouldRedraw = true;
    bool focused           = false;
    std::optional<FrameCharSet> frameChars;
    std::optional<std::wstring> title;
};

class CtrlPanel;

struct CtrlItem {
    inline CtrlItem(std::wstring label, size_t width)
        : label(label), width(width) {}
    inline virtual ~CtrlItem() {}

    std::wstring label;
    size_t width;
    bool focused = false;

    virtual std::wstring getContent() = 0;

    virtual void onCharInput(CtrlPanel& ctrlPanel, wchar_t ch) = 0;

    inline void drawContent(size_t width) {
        stdoutwss << getContent().substr(0, width);
    }

    inline void draw(size_t row, size_t col) {
        cursor::move(row, col);
        stdoutwss << ui::blue << (focused ? L"> " : L"  ") << ui::reset
                  << label;
        drawContent(width - 4 - label.size());
    }
};

struct SwitchCtrlItem final : public CtrlItem {
    inline SwitchCtrlItem(std::wstring label, size_t width, bool active)
        : CtrlItem(label, width), active(active) {}

    virtual inline std::wstring getContent() override {
        return (active) ? L": YES" : L": NO ";
    }

    virtual inline void
    onCharInput([[maybe_unused]] CtrlPanel& ctrlPanel, wchar_t ch) override {
        if (ch == L' ') {
            active = !active;
        }
    }

    bool active;
};

struct ButtonCtrlItem final : public CtrlItem {
    std::function<void(CtrlPanel&)> onPress;

    inline ButtonCtrlItem(
        std::wstring label,
        size_t width,
        std::function<void(CtrlPanel&)> onPress = [](CtrlPanel&) {})
        : CtrlItem(label, width), onPress(onPress) {}

    virtual inline std::wstring getContent() override { return L""; }

    virtual inline void onCharInput(CtrlPanel& ctrlPanel, wchar_t ch) override {
        if (ch == L' ') {
            onPress(ctrlPanel);
        }
    }
};

class CtrlPanel final : public IPanel {
  public:
    CtrlPanel(PanelDim dim, util::Sender<CallbackEvent> uiCallbackSender);

    virtual void setDim(PanelDim dim) override;

    /** @brief Pass the input to highlighted item */
    inline void onCharInput(wchar_t input) {
        items[curItem]->onCharInput(*this, input);
    }

    void onArrowEvent(CallbackEvent::ArrowPress event);

    void drawContent() override;

    /** @brief Get library names based on whether the switches are on or off */
    std::pair<std::optional<std::string>, std::optional<std::string>>
    getLibNames();

  private:
    inline void sendStartGame() {
        uiCallbackSender.send(CallbackEvent::newStartGame());
    }

    inline void sendQuit() {
        uiCallbackSender.send(CallbackEvent::newShutdown());
    }

    wchar_t ch;
    std::vector<std::unique_ptr<CtrlItem>> items;
    size_t curItem = 0;

    util::Sender<CallbackEvent> uiCallbackSender;
};

class LogPanel final : public IPanel {
  public:
    LogPanel(PanelDim dim) : IPanel(dim, true, FrameCharSet{}, L"Game Log") {}

    void drawContent() override;

    void clearLogs();
    template <typename... T> void push(T&&... args) {
        logs.emplace_back(std::forward<T>(args)...);
        if (logs.size() > 200)
            logs.pop_front();
    }

  private:
    std::deque<std::pair<std::wstring, size_t>> logs;
};

class BoardPanel final : public IPanel {
  public:
    BoardPanel(
        PanelDim dim,
        TA::UltraBoard ub,
        util::Sender<CallbackEvent> uiCallbackSender)
        : IPanel(dim, true, FrameCharSet{}, L"Game Board"), ub(ub),
          uiCallbackSender(uiCallbackSender) {}

    void setBoard(TA::UltraBoard ub);

    void drawContent() override;

    inline void onCharInput(wchar_t input) {
        if (input == L' ') {
            if (focused == true && userTurn == true) {
                uiCallbackSender.send(CallbackEvent::newUserMove(
                    cursorAt.second, cursorAt.first));
                userTurn = false;
            }
        }
    }

    inline void onArrowEvent(CallbackEvent::ArrowPress event) {
        switch (event.value) {
        case CallbackEvent::ArrowPress::Up:
            if (cursorAt.second != 0)
                cursorAt.second -= 1;
            break;
        case CallbackEvent::ArrowPress::Down:
            if (cursorAt.second != 8)
                cursorAt.second += 1;
            break;
        case CallbackEvent::ArrowPress::Right:
            if (cursorAt.first != 8)
                cursorAt.first += 1;
            break;
        case CallbackEvent::ArrowPress::Left:
            if (cursorAt.first != 0)
                cursorAt.first -= 1;
            break;
        }
    }

    inline void setUserTurn(bool turn) { userTurn = turn; }

  private:
    TA::UltraBoard ub;
    std::pair<int, int> cursorAt{0, 0};
    bool userTurn = false;
    util::Sender<CallbackEvent> uiCallbackSender;
};
} // namespace ui

#endif /* ifndef PANELS_H */
